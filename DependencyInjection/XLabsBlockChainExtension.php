<?php

namespace XLabs\BlockChainBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class XLabsBlockChainExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('xlabs_blockchain_config', $config);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        /*$resources = $container->getParameter('twig.form.resources');
        $container->setParameter('twig.form.resources', array_merge(array(
            'XLabsForumBundle:CustomFormFields:single_image_upload.html.twig',
            'XLabsForumBundle:CustomFormFields:emoji_text_input.html.twig',
            'XLabsForumBundle:CustomFormFields:emoji_textarea_input.html.twig',
        ), $resources));*/
    }
}
