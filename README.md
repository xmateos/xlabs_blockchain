A blockchain bundle.

## Installation ##

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require xlabs/blockchainbundle
```

This bundle depends on "xlabs/blockchainbundle". Make sure to set it up too.

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new XLabs\BlockChainBundle\XLabsBlockChainBundle(),
    ];
}
```
