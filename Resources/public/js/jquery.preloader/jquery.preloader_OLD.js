// jQuery Preloader plugin v1.0
// (c) 2015 Xavi Mateos - http://es.linkedin.com/pub/xavi-mateos/12/8ab/529
// License: http://www.opensource.org/licenses/mit-license.php
(function( $ ) {
    
    $.fn.preloader = function(method) {
        
        var loadCSSIfNotAlreadyLoaded = function(){
            var ss = document.styleSheets;
            var pluginCSS = '/bundles/mmcore/plugins/jquery.preloader/jquery.preloader.css';
            for (var i = 0, max = ss.length; i < max; i++) {
                if (ss[i].href != null && ss[i].href.indexOf(pluginCSS) > -1)
                    return;
            }
            var link = document.createElement("link");
            link.rel = "stylesheet";
            link.href = pluginCSS;

            document.getElementsByTagName("head")[0].appendChild(link);
        };
    
        var methods = {
            init : function(options){
                this.preloader.settings = $.extend({}, this.preloader.defaults, options);
                settings = this.preloader.settings;

                var preloaderImg = new Image();
                preloaderImg.src = settings.loading_animation;

                loadCSSIfNotAlreadyLoaded();

                return this.each(function(i) {
                    var obj = $(this);

                    //var wrapper = $('<div/>');
                    //wrapper.addClass('jqp_wrapper');
                    //obj.wrap(wrapper);

                    var background = $('<div/>');
                    background.addClass('jqp_background');

                    var preloader = $('<div/>');
                    preloader.addClass('jqp_preloader');
                    var pSub = $('<div/>');
                    pSub.addClass('jqp_preloader_inner');

                    var image = $('<img/>');
                    image.addClass('jqp_img');
                    image.attr('width', preloaderImg.width);
                    image.attr('height', preloaderImg.height);
                    image.css({
                        'width': preloaderImg.width,
                        'height': preloaderImg.height
                    });

                    image.attr('src', settings.loading_animation).load(function(){

                    });
                    var text = settings.loading_text;
                    var dots = '...';

                    var textHTML = text + dots;
                    var characters = [];
                    if(settings.animate_text)
                    {
                        characters = text.split('');
                        textHTML = '';
                        $.each(characters, function (i, el) {
                            if(el == ' ')
                            {
                                el = '&nbsp;';
                            }
                            textHTML += '<span>' + el + '</span>';
                        });
                        characters = dots.split('');
                        dotsHTML = '';
                        $.each(characters, function (i, el) {
                            dotsHTML += '<span>' + el + '</span>';
                        });
                        textHTML = '<span class="jqp_animated_text">' + textHTML + dotsHTML + '</span>';
                    } else {
                        if(settings.animate_dots)
                        {
                            characters = dots.split('');
                            dotsHTML = '';
                            $.each(characters, function (i, el) {
                                dotsHTML += '<span>' + el + '</span>';
                            });
                            dotsHTML = '<span class="jqp_animated_text">' + dotsHTML + '</span>';
                            textHTML = text + dotsHTML
                        }
                    }

                    var textHTML = '<br /><p>' + textHTML + '</p>';
                    pSub
                        .append(image)
                        .append(textHTML);
                    preloader.append(pSub);

                    // if plugin already exists, replace objects
                    if(obj.find('.jqp_background').length)
                    {
                        obj.find('.jqp_background').remove();
                        obj.find('.jqp_preloader').remove();
                    }
                    obj
                        .append(background)
                        .append(preloader);

                    methods.setBlinkCSS(obj);

                    if(settings.force_container)
                    {
                        obj.css({
                            'position': 'relative'
                        });
                    }
                });
            },
            setBlinkCSS : function(obj){
                var preloader = obj.find('.jqp_preloader');
                var animated_container = preloader.find('span.jqp_animated_text');
                var animated_items = animated_container.children('span');

                var currentElement;
                var delay = 0;
                animated_items.each(function(i, el) {
                    delay += settings.animation_delay;
                    $(el).css({
                        'animation-delay': delay + 's'
                    });
                });
                animated_items.each(function(i, el) {
                    $(el).css({
                        'animation-duration': delay + 's'
                    });
                });
            },
            enable : function(e){
                var obj = $(this);
                var preloader = obj.find('.jqp_preloader');
                var background = obj.find('.jqp_background');
                !preloader.hasClass('active') ? preloader.addClass('active') : false;
                !background.hasClass('active') ? background.addClass('active') : false;
                /*background.css({
                    'height': obj.outerHeight(true) + 'px'
                });*/
            },
            disable : function(e){
                var obj = $(this);
                var preloader = obj.find('.jqp_preloader');
                var background = obj.find('.jqp_background');
                preloader.hasClass('active') ? preloader.removeClass('active') : false;
                background.hasClass('active') ? background.removeClass('active') : false;
            }
        };

        if ( methods[method] )
        {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else {
            if ( typeof method === 'object' || ! method )
            {
                return methods.init.apply( this, arguments );
            } else {
                $.error( 'Method ' +  method + ' does not exist on jQuery.preloader' );
            }
        }
    };

    $.fn.preloader.defaults = {
        'loading_animation' : '/media/assets/stiffia/images/preloader.png',
        'loading_text'      : 'LOADING',
        'animate_dots'      : true,
        'animate_text'      : true, // if true, it will automatically set 'animate_dots' to true
        'animation_delay'   : 0.2, // delay between letter animation
        'force_container'   : false // will force 'position: relative' to container when it doesnt adjust properly
    };

    $.fn.preloader.settings = {};
    
})( jQuery );