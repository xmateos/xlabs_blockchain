<?php

namespace XLabs\BlockChainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class BlockChainController extends Controller
{
    /**
     * @Route("/", name="xlabs_blockchain_index", defaults={"topic_id": false}, options={"expose"=true})
     */
    public function indexAction(Request $request, RouterInterface $router, EventDispatcherInterface $event_dispatcher)
    {
        return new Response('ok');
    }
}
